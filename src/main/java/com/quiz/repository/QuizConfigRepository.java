package com.quiz.repository;

import org.springframework.stereotype.Repository;
import com.quiz.model.QuizConfig;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author aleks
 */
@Repository("QuizConfigRepository")
public interface QuizConfigRepository extends JpaRepository<QuizConfig, Long> {

}
