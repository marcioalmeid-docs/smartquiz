package com.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quiz.model.Question;
import com.quiz.model.Quiz;

/**
 *
 * @author aleks
 */
@Repository("QuestionRepository")
public interface QuestionRepository extends JpaRepository<Question, Long> {

    /**
     *
     * @param quiz
     * @return
     */
    int countByQuiz(Quiz quiz);

    /**
     *
     * @param quiz
     * @return
     */
    int countByQuizAndIsValidTrue(Quiz quiz);

    /**
     *
     * @param quiz
     * @return
     */
    List<Question> findByQuizOrderByOrderAsc(Quiz quiz);

    /**
     *
     * @param quiz
     * @return
     */
    List<Question> findByQuizAndIsValidTrueOrderByOrderAsc(Quiz quiz);
}
