package com.quiz.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.quiz.model.TokenModel;

/**
 *
 * @author aleks
 * @param <T>
 */
@NoRepositoryBean
public interface TokenRepository<T extends TokenModel> extends CrudRepository<T, Long> {

    /**
     *
     * @param token
     * @return
     */
    T findByToken(String token);

    /**
     *
     * @param date
     */
    @Modifying
	void deletePreviousTo(Date date);
}