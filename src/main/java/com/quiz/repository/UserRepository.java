package com.quiz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quiz.model.User;

/**
 *
 * @author aleks
 */
@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     *
     * @param email
     * @return
     */
    User findByEmail(String email);

    /**
     *
     * @param username
     * @return
     */
    User findByUsername(String username);
    
    void delete(User user);
}
