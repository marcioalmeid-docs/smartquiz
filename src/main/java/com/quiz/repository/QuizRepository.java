package com.quiz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.quiz.model.Quiz;
import com.quiz.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author aleks
 */
@Repository("QuizRepository")
public interface QuizRepository extends PagingAndSortingRepository<Quiz, Long>, JpaRepository<Quiz, Long> {

    /**
     *
     * @param pageable
     * @return
     */
    Page<Quiz> findByIsPublishedTrue(Pageable pageable);

    /**
     *
     * @param user
     * @param pageable
     * @return
     */
    Page<Quiz> findByCreatedBy(User user, Pageable pageable);

    /**
     *
     * @param name
     * @param pageable
     * @return
     */
    @Query("select q from Quiz q where q.name like %?1%")
	Page<Quiz> searchByName(String name, Pageable pageable);
    
    
    Quiz findByName(String name);
}
