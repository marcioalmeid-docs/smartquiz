package com.quiz.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.quiz.model.ForgotPasswordToken;

/**
 *
 * @author aleks
 */
@Repository
public interface ForgotPasswordTokenRepository extends TokenRepository<ForgotPasswordToken> {

    /**
     *
     * @param date
     */
    @Modifying
	@Query("delete from ForgotPasswordToken t where t.expirationDate <= ?1")
        @Override
	void deletePreviousTo(Date date);
}
