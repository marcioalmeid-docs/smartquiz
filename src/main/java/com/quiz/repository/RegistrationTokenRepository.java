package com.quiz.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.quiz.model.RegistrationToken;

/**
 *
 * @author aleks
 */
@Repository
public interface RegistrationTokenRepository extends TokenRepository<RegistrationToken> {

    /**
     *
     * @param date
     */
    @Modifying
	@Query("delete from RegistrationToken t where t.expirationDate <= ?1")
        @Override
	void deletePreviousTo(Date date);
}
