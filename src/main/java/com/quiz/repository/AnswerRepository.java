package com.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quiz.model.Answer;
import com.quiz.model.Question;

/**
 *
 * @author aleks
 */
@Repository("AnswerRepository")
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    /**
     *
     * @param question
     * @return
     */
    int countByQuestion(Question question);

    /**
     *
     * @param question
     * @return
     */
    List<Answer> findByQuestionOrderByOrderAsc(Question question);

}
