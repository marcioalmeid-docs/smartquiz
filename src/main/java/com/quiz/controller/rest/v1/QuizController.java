package com.quiz.controller.rest.v1;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.controller.utils.RestVerifier;
import com.quiz.model.Answer;
import com.quiz.model.AuthenticatedUser;
import com.quiz.model.Question;
import com.quiz.model.Quiz;
import com.quiz.model.QuizConfig;
import com.quiz.model.User;
import com.quiz.model.support.Response;
import com.quiz.model.support.Result;
import com.quiz.service.QuestionService;
import com.quiz.service.QuizService;
import com.quiz.service.UserService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping(QuizController.ROOT_MAPPING)
@CrossOrigin(origins = "http://localhost:4200")
public class QuizController {

	public static final String ROOT_MAPPING = "/api/quizes";

	private static final Logger logger = LoggerFactory.getLogger(QuizController.class);

	@Autowired
	private QuizService quizService;

	@Autowired
	private UserService userService;

	@Autowired
	private QuestionService questionService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Page<Quiz> findAll(Pageable pageable,
			@RequestParam(required = false, defaultValue = "false") Boolean published) {

		if (published) {
			return quizService.findAllPublished(pageable);
		} else {
			return quizService.findAll(pageable);
		}
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Page<Quiz> searchAll(Pageable pageable, @RequestParam(required = true) String filter,
			@RequestParam(required = false, defaultValue = "false") Boolean onlyValid) {

		return quizService.search(filter, pageable);
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.CREATED)
	public Quiz save(@AuthenticationPrincipal AuthenticatedUser user, @Valid Quiz quiz, BindingResult result) {

		logger.debug("The Quiz " + quiz.getName() + " is going to be created");

		RestVerifier.verifyModelResult(result);

		return quizService.save(quiz, user.getUser());
	}

	public Quiz saveQuiz(User user, Quiz quiz, QuizConfig quizConfig, BindingResult result) {

		RestVerifier.verifyModelResult(result);

		return quizService.saveQuiz(quiz, user.getUser(), quizConfig);
	}

	@RequestMapping(value = "/{quiz_id}", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Quiz find(@PathVariable Long quiz_id) {

		return quizService.find(quiz_id);
	}

	@RequestMapping(value = "/{quiz_id}", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public Quiz update(@PathVariable Long quiz_id, @Valid Quiz quiz, BindingResult result) {

		RestVerifier.verifyModelResult(result);

		quiz.setId(quiz_id);
		return quizService.update(quiz);
	}

	@RequestMapping(value = "/{quiz_id}", method = RequestMethod.DELETE)
//	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long quiz_id) {
		Quiz quiz = quizService.find(quiz_id);
		quizService.delete(quiz);
	}

	@RequestMapping(value = "/{quiz_id}/questions", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public List<Question> findQuestions(@PathVariable Long quiz_id,
			@RequestParam(required = false, defaultValue = "false") Boolean onlyValid) {

		Quiz quiz = quizService.find(quiz_id);

		if (onlyValid) {
			return questionService.findValidQuestionsByQuiz(quiz);
		} else {
			return questionService.findQuestionsByQuiz(quiz);
		}

	}

	@RequestMapping(value = "/{quiz_id}/publish", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void publishQuiz(@PathVariable long quiz_id) {
		Quiz quiz = quizService.find(quiz_id);
		quizService.publishQuiz(quiz);
	}

	@RequestMapping(value = "/{quiz_id}/submitAnswers", method = RequestMethod.POST)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Result playQuiz(@PathVariable long quiz_id, @RequestBody List<Response> answersBundle) {
		Quiz quiz = quizService.find(quiz_id);
		return quizService.checkAnswers(quiz, answersBundle);
	}

	@GetMapping(value = "/edit/{id}")
	public Quiz getQuiz(@PathVariable Long id) {
		Quiz quiz = quizService.find(id);
		System.out.println("chekcing quiz: " + quiz.getName());
		return quiz;
	}

	@GetMapping(value = "/find/{name}")
	public Quiz getQuiz(@PathVariable String name) {
		Quiz quiz = quizService.findByName(name);
		System.out.println("chekcing quiz: " + quiz.getName());
		return quiz;
	}

	@GetMapping(value = "/")
	public List<Quiz> findAll() {
		List<Quiz> quizes = quizService.findAll();
		return quizes;
	}

//        @PostMapping("/save/")
//        public ResponseEntity <Quiz> postQuiz(@RequestBody Quiz quiz){
//        	System.out.println("Print quiz: "+ quiz.getName());
//        	System.out.println("Print no of questions: "+ quiz.getQuestions().size());
//        	System.out.println("Print no of answers in quiz : "+ quiz.getQuestions().get(0).getAnswers().size());
//         User user1 =    userService.find(1l);
//         System.out.println(user1.getUsername());
//        	quizService.save(quiz, user1);     	
//        	return ResponseEntity.ok(quiz);
//        }

	@PostMapping("/save/")
	@PreAuthorize("permitAll")
	public ResponseEntity<Quiz> postQuiz(@RequestBody Quiz quiz) {
		System.out.println("Print quiz name: " + quiz.getName());
		System.out.println("Print no of questions: " + quiz.getQuestions().size());
		System.out.println("Print no of answers in quiz : " + quiz.getQuestions().get(0).getAnswers().size());
		try {
			for (Question q : quiz.getQuestions()) {
				if (q != null) {
					q.setOrder(0);// NULO
					//q.setCorrectAnswer(new Answer());// DISABLED
					q.setQuiz(quiz);// WAS NULO
				  System.out.println("CORRECT ANSWER"+q.getCorrectAnswer());		
					System.out.printf(
							"Question Id: %d \t name: %s \t order: %d \t correct answer id:Null \t Quiz id:%d \t created date: %s \n",
							getNotNull(q.getId()), getNotNull(q.getText()), getNotNull(q.getOrder()),
							getNotNull(q.getQuiz().getId()), getNotNull(q.getCreatedDate().toString()));
				}
				
				for (Answer a : q.getAnswers()) {
					a.setQuestion(q);
					//a.setCorrect(false);//NEED TO BE CORRECTED
					System.out.println("TEXT: "+a.getText()+" id"+a.getId()+" order"+a.getOrder()+" date"+a.getCreatedDate());
					/*
					 * if (a != null)
						System.out.printf("\n Answer id: %d \t  text: %s \t CreatedDate: %s \t Order %d \t isCorrect %b \t Question id: %d ",getNotNull(a.getId()),						getNotNull(a.getText()),
								getNotNull(a.getCreatedDate()),			getNotNull(a.getOrder()),				a.getQuestion().getId());
					*/
				}

			}
		} catch (Exception e) {
			System.err.println(e.getCause());
			e.printStackTrace();
		}

		 quizService.saveQuiz2(quiz);
		return ResponseEntity.ok(quiz);
	}

	private Object getNotNull(Object o) {
		System.out.println("Element " + o);
		if (o == null) {
			System.out.println("NULO");
			return "NULO";
		}
		if (o instanceof String) {
			return (o != null ? o : "nulo");
		}
		if (o instanceof Long) {
			return (o != null ? o : -1);
		} else {
			return (o != null ? o : "nulo");
		}

	}

	@DeleteMapping("/delete/{id}") 
	@ResponseStatus(HttpStatus.OK)
	public void deleteQuiz(@PathVariable Long id) {
		Quiz quiz = quizService.find(id);
		quizService.delete(quiz);
	}
	
}