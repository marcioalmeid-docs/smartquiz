package com.quiz.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quiz.exceptions.ActionRefusedException;
import com.quiz.exceptions.InvalidParametersException;
import com.quiz.exceptions.ResourceUnavailableException;
import com.quiz.exceptions.UnauthorizedActionException;
import com.quiz.model.Question;
import com.quiz.model.Quiz;
import com.quiz.model.QuizConfig;
import com.quiz.model.User;
import com.quiz.model.support.Response;
import com.quiz.model.support.Result;
import com.quiz.repository.QuizConfigRepository;
import com.quiz.repository.QuizRepository;

@Service("QuizService")
@Transactional
public class QuizServiceImpl implements QuizService {

	private static final Logger logger = LoggerFactory.getLogger(QuizServiceImpl.class);
	private QuizRepository quizRepository;
	private QuizConfigRepository quizConfigRepository;

	private QuestionService questionService;

	@Autowired
	public QuizServiceImpl(QuizRepository quizRepository, QuestionService questionService) {
		this.quizRepository = quizRepository;
		this.questionService = questionService;
	}

	@Override
	public Quiz save(Quiz quiz, User user) {
		quiz.setCreatedBy(user);
		return quizRepository.save(quiz);
	}
	
	@Override
	public Quiz saveQuiz2(Quiz quiz) {
		return quizRepository.save(quiz);
	}
	
	@Override
	public Quiz saveQuiz(Quiz quiz, User user, QuizConfig quizConfig) {
		quizConfig.setQuiz(quiz);
		this.saveQuizConfig(quizConfig);
		quiz.setCreatedBy(user);
		return quizRepository.save(quiz);
	}
	
	public QuizConfig saveQuizConfig(QuizConfig quizConfig) {
		return quizConfigRepository.save(quizConfig);
	}

	@Override
	public Page<Quiz> findAll(Pageable pageable) {
		return quizRepository.findAll(pageable);
	}

	@Override
	public Page<Quiz> findAllPublished(Pageable pageable) {
		return quizRepository.findByIsPublishedTrue(pageable);
	}
        
        @Override
        public List<Quiz> findAll() throws ResourceUnavailableException {
           return quizRepository.findAll();
        }

	@Override
	public Quiz find(Long id) throws ResourceUnavailableException {
		Quiz quiz = quizRepository.getOne(id);

		if (quiz == null) {
			logger.error("Quiz " + id + " not found");
			throw new ResourceUnavailableException("Quiz " + id + " not found");
		}

		return quiz;
	}

	@Override
	public Quiz update(Quiz newQuiz) throws UnauthorizedActionException, ResourceUnavailableException {
		Quiz currentQuiz = find(newQuiz.getId());

		mergeQuizes(currentQuiz, newQuiz);
		return quizRepository.save(currentQuiz);
	}

	@Override
	public void delete(Quiz quiz) throws ResourceUnavailableException, UnauthorizedActionException {
		quizRepository.delete(quiz);
	}

	private void mergeQuizes(Quiz currentQuiz, Quiz newQuiz) {
		currentQuiz.setName(newQuiz.getName());
		currentQuiz.setDescription(newQuiz.getDescription());
	}

	@Override
	public Page<Quiz> search(String query, Pageable pageable) {
		return quizRepository.searchByName(query, pageable);
	}

	@Override
	public Page<Quiz> findQuizesByUser(User user, Pageable pageable) {
		return quizRepository.findByCreatedBy(user, pageable);
	}

	@Override
	public Result checkAnswers(Quiz quiz, List<Response> answersBundle) {
		Result results = new Result();

		for (Question question : quiz.getQuestions()) {
			boolean isFound = false;

			if (!question.getIsValid()) {
				continue;
			}

			for (Response bundle : answersBundle) {
				if (bundle.getQuestion().equals(question.getId())) {
					isFound = true;
					results.addAnswer(questionService.checkIsCorrectAnswer(question, bundle.getSelectedAnswer()));
					break;
				}
			}

			if (!isFound) {
				throw new InvalidParametersException("No answer found for question: " + question.getText());
			}
		}

		return results;
	}

	@Override
	public void publishQuiz(Quiz quiz) {
		int count = questionService.countValidQuestionsInQuiz(quiz);

		if (count > 0) {
			quiz.setIsPublished(true);
			quizRepository.save(quiz);
		} else {
			throw new ActionRefusedException("The quiz doesn't have any valid questions");
		}
	}
	
	@Override
	public Quiz findByName(String name) throws ResourceUnavailableException {
		Quiz quiz = quizRepository.findByName(name);

		if (quiz == null) {
			logger.error("The user " + name + " doesn't exist");
			throw new ResourceUnavailableException("The user " + name + " doesn't exist");
		}

		return quiz;
	}

}
