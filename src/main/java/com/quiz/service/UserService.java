package com.quiz.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.quiz.exceptions.ResourceUnavailableException;
import com.quiz.exceptions.UnauthorizedActionException;
import com.quiz.exceptions.UserAlreadyExistsException;
import com.quiz.model.User;
import java.util.List;

public interface UserService extends UserDetailsService {
	User saveUser(User user) throws UserAlreadyExistsException;

	User find(Long id) throws ResourceUnavailableException;;

	User findByEmail(String email) throws ResourceUnavailableException;

	User findByUsername(String username) throws ResourceUnavailableException;

	User updatePassword(User user, String password) throws ResourceUnavailableException;

	void delete(Long user_id) throws UnauthorizedActionException, ResourceUnavailableException;

	User setRegistrationCompleted(User user) throws ResourceUnavailableException;

	boolean isRegistrationCompleted(User user) throws ResourceUnavailableException;
        
    List<User> findAll();

}