package com.quiz.service.usermanagement;

import com.quiz.model.User;

public interface RegistrationService {
	User startRegistration(User user);

	void continueRegistration(long userId, String token);

	boolean isRegistrationCompleted(User user);
}
