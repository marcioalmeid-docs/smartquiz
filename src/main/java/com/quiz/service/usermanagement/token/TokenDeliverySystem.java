package com.quiz.service.usermanagement.token;

import org.springframework.scheduling.annotation.Async;

import com.quiz.model.TokenModel;
import com.quiz.model.TokenType;
import com.quiz.model.User;

public interface TokenDeliverySystem {
	@Async
	void sendTokenToUser(TokenModel token, User user, TokenType tokenType);
}
