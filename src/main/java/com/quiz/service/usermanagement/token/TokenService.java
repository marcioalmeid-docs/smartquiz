package com.quiz.service.usermanagement.token;

import java.util.Date;

import com.quiz.exceptions.InvalidTokenException;
import com.quiz.model.TokenModel;
import com.quiz.model.User;

public interface TokenService<T extends TokenModel> {
	T generateTokenForUser(User user);

	void validateTokenForUser(long userId, String token) throws InvalidTokenException;

	void invalidateToken(String token);

	void invalidateExpiredTokensPreviousTo(Date date);
}
