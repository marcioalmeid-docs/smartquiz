package com.quiz.service.usermanagement.utils;

public interface TokenGenerator {

	String generateRandomToken();

}