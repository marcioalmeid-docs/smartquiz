package com.quiz.service;

import java.util.List;

import com.quiz.exceptions.ResourceUnavailableException;
import com.quiz.exceptions.UnauthorizedActionException;
import com.quiz.model.Answer;
import com.quiz.model.Question;

public interface AnswerService {
	Answer save(Answer answer) throws UnauthorizedActionException;

	Answer find(Long id) throws ResourceUnavailableException;

	Answer update(Answer newAnswer) throws UnauthorizedActionException, ResourceUnavailableException;

	void delete(Answer answer) throws UnauthorizedActionException, ResourceUnavailableException;

	List<Answer> findAnswersByQuestion(Question question);

	int countAnswersInQuestion(Question question);
}
