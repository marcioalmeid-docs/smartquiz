package com.quiz.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.quiz.exceptions.ResourceUnavailableException;
import com.quiz.exceptions.UnauthorizedActionException;
import com.quiz.model.Quiz;
import com.quiz.model.QuizConfig;
import com.quiz.model.User;
import com.quiz.model.support.Response;
import com.quiz.model.support.Result;

public interface QuizService {
	Quiz save(Quiz quiz, User user);
	
	Quiz saveQuiz(Quiz quiz, User user, QuizConfig quizConfig);

	Page<Quiz> findAll(Pageable pageable);

	Page<Quiz> findAllPublished(Pageable pageable);

	Page<Quiz> findQuizesByUser(User user, Pageable pageable);

	Quiz find(Long id) throws ResourceUnavailableException;

	Quiz update(Quiz quiz) throws ResourceUnavailableException, UnauthorizedActionException;

	void delete(Quiz quiz) throws ResourceUnavailableException, UnauthorizedActionException;

	Page<Quiz> search(String query, Pageable pageable);

	Result checkAnswers(Quiz quiz, List<Response> answersBundle);

	void publishQuiz(Quiz quiz);

    public List<Quiz> findAll();
    
    Quiz findByName(String name);
    
    Quiz saveQuiz2(Quiz quiz);
}
