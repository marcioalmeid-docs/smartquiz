package com.quiz.model;

public enum TokenType {
	REGISTRATION_MAIL, FORGOT_PASSWORD
}
