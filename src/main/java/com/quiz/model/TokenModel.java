package com.quiz.model;

import java.util.Date;
import javax.persistence.FetchType;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class TokenModel extends BaseModel {

	@NotNull
	String token;

	@OneToOne(fetch = FetchType.EAGER)
	User user;

        @Temporal(javax.persistence.TemporalType.DATE)
	Date expirationDate;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return getToken();
	}

}
