package com.quiz.model;

public interface UserOwned {
	User getUser();
}
