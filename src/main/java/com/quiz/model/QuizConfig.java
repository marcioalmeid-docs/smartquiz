package com.quiz.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "quiz_config")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class QuizConfig extends BaseModel{
	
	private boolean allowBack;
	private boolean allowReview;
	private boolean autoMove;
	private int duration;
	private int pageSize;
	private boolean requredAll;
	private boolean richText;
	private boolean shuffleQuestions;
	private boolean shuffleOptions;
	private boolean showClock;
	private boolean showPager;
	
	@OneToOne
	private Quiz quiz;
	
	public Quiz getQuiz() {
		return quiz;
	}
	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}
	public boolean isAllowBack() {
		return allowBack;
	}
	public void setAllowBack(boolean allowBack) {
		this.allowBack = allowBack;
	}
	public boolean isAllowReview() {
		return allowReview;
	}
	public void setAllowReview(boolean allowReview) {
		this.allowReview = allowReview;
	}
	public boolean isAutoMove() {
		return autoMove;
	}
	public void setAutoMove(boolean autoMove) {
		this.autoMove = autoMove;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isRequredAll() {
		return requredAll;
	}
	public void setRequredAll(boolean requredAll) {
		this.requredAll = requredAll;
	}
	public boolean isRichText() {
		return richText;
	}
	public void setRichText(boolean richText) {
		this.richText = richText;
	}
	public boolean isShuffleQuestions() {
		return shuffleQuestions;
	}
	public void setShuffleQuestions(boolean shuffleQuestions) {
		this.shuffleQuestions = shuffleQuestions;
	}
	public boolean isShuffleOptions() {
		return shuffleOptions;
	}
	public void setShuffleOptions(boolean shuffleOptions) {
		this.shuffleOptions = shuffleOptions;
	}
	public boolean isShowClock() {
		return showClock;
	}
	public void setShowClock(boolean showClock) {
		this.showClock = showClock;
	}
	public boolean isShowPager() {
		return showPager;
	}
	public void setShowPager(boolean showPager) {
		this.showPager = showPager;
	}
}
