package com.quiz.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Temporal;

@Entity
@Table(name = "answer")
//Jackson tries to serialize a field called correct from a getter called isCorrect (which is a conventional bean property name).
//@JsonIgnoreProperties("correct") 
public class Answer extends BaseModel implements UserOwned {

	@Size(min = 1, max = 50, message = "The answer should be less than 50 characters")
	@NotNull(message = "No answer text provided.")
	private String text;

	@ManyToOne
	@JsonIgnore
	private Question question;

	@Column(name = "a_order")
	private Integer order;
	
	//@JsonIgnore
	//@Column(name = "isCorrect", columnDefinition = "int default 0")
	//@NotNull
	//private Boolean isCorrect;

	@Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
	private Calendar createdDate;

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	@JsonIgnore
	public User getUser() {
		return question.getUser();
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
	

	/*public boolean isCorrect() {
		return isCorrect;
	}

	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}*/

	@Override
	public String toString() {
		return "Answer [text=" + text + ", question=" + question + ", order=" + order + ", createdDate=" + createdDate
				+ "]";
	}

}
