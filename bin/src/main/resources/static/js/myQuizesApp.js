(function() {

	var app = angular.module("myQuizesApp", []);

	var myQuizesCtrl = function($scope, $http) {	  
	
		$scope.pagination = {
			pageNumber: 0,
			morePagesAvailable: true
		};
		
		$scope.loadNextPage = function(quizName, quizDescription) {
		
			if ($scope.pagination.morePagesAvailable) {
				$http.get("/api/users/" + $scope.userId + "/quizes?page=" + $scope.pagination.pageNumber)
					.then(
						function(response) {
							if ($scope.quizes == undefined) {
								$scope.quizes = response.data.content;
							} else {
								$scope.quizes = $scope.quizes.concat(response.data.content);
							}
							
							$scope.pagination.morePagesAvailable = !response.data.last;
							$scope.pagination.pageNumber++;
						}, 
						function(reason) {
							$scope.error = "Could not fetch the data.";
						}
					);
			}
		}
		
		$scope.deleteQuiz = function(quizId) {
			if (quizId == 0)
				return;

			$http.delete("/api/quizes/" + quizId)
			.then(
					function(response) {
						for (var i = 0; i < $scope.quizes.length; i++) {
						    if ($scope.quizes[i].id == quizId) {
						    	$scope.quizes.splice(i, 1);
						    	break;
						    }
						}
					}, 
					function(reason) {
						console.log(reason.data);
					}
			);
		}
	
		$scope.loadNextPage();
		
	};

	app.controller("MyQuizesCtrl", ["$scope", "$http", myQuizesCtrl]);

}());